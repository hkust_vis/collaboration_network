import networkx as nx

import json
import csv
from networkx.readwrite import json_graph


path = 'data/cse.gml';
H=nx.read_gml(path);

for index in H.node:
    node = H.node[index];
    label = node['label'];
    arr = label.split(':');
    node['uid'] = arr[0];
    node['dept'] = arr[1];
    node['address']= arr[2];
    node["collaMap"] = {};
    node["total"] = 0;
    node["collaboration"]  = 0;
    links = H.edges(index);
    for linkindex in links:

        s = linkindex[0];
        t = linkindex[1];
        weight = int(H.get_edge_data(s,t)["weight"]);
        node["total"] += weight;

        if s != t:
            node["collaboration"] += weight;
            node["collaMap"][t] = weight;

#     if hashMap.__contains__(node['dept']):
#         arrindex = hashMap[node['dept']];
#         array[arrindex]+=1;
#     else:
#         hashMap[node['dept']] = i;
#         array[i]+=1;
#         i+=1;
#
# for node in hashMap:
#     print [node,hashMap[node]];


data = json_graph.node_link_data(H)
writeFile=file("cse.json","wb")
json.dump(data,writeFile);
