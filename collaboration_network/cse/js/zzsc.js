
(function ($) {
	$(function () {
		nav(); //top naviation
	});
	
	function nav() {
        var $liCur = $(".nav-box>ul>li.cur"),
            curP = $liCur.position().left,
            curW = $liCur.outerWidth(true);
		var $slider = $(".nav-line"),
			$targetEle = $(".nav-box>ul>li"),
			$navBox = $(".nav-box");
		$slider.stop(true, true).animate({
			"left" : curP,
			"width" : curW
		});
		$targetEle.mouseenter(function () {
			var $_parent = $(this);//.parent(),
			_width = $_parent.outerWidth(true),
			posL = $_parent.position().left;
			$slider.stop(true, true).animate({
				"left" : posL,
				"width" : _width
			}, "fast");
		});
        $targetEle.mousedown(function () {
            var $_parent = $(this);
            var $_liCur = $(".nav-box>ul>li.cur");
            $_liCur.removeClass();
            $_parent.addClass('cur');
        });
		$navBox.mouseleave(function (cur, wid) {
            var $_liCur = $(".nav-box>ul>li.cur"),
                _curP = $_liCur.position().left,
                _curW = $_liCur.outerWidth(true);
			$slider.stop(true, true).animate({
				"left" : _curP,
				"width" : _curW
			}, "fast");
		});
	};
	
})(jQuery);