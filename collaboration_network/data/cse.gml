graph [
  node [
    id 0
    label "10915:CSE:dyyeung"
    name "YEUNG, Dit-Yan"
    area "AI"
    url "dyyeung.png"
  ]
  node [
    id 1
    label "11432:CSE:fred"
    name "LOCHOVSKY, Frederick H."
    area "DB"
    url "fred.png"
  ]
  node [
    id 2
    label "10886:CSE:dlee"
    name "LEE, Dik-Lun"
    area "DB"
    url "dlee.png"
  ]
  node [
    id 3
    label "12382:CSE:liuyh"
    name "liuyh"
    area ""
    url ""
  ]
  node [
    id 4
    label "11529:CSE:helens"
    name "SHEN, Helen C."
    area "VG"
    url "helens.png"
  ]
  node [
    id 5
    label "11474:CSE:golin"
    name "GOLIN, Mordecai J."
    area "TH"
    url "golin.png"
  ]
  node [
    id 6
    label "12805:CSE:muppala"
    name "MUPPALA, Jogesh K."
    area "NE"
    url "muppala.png"
  ]
  node [
    id 7
    label "12312:CSE:leichen"
    name "CHEN, Lei"
    area "DB"
    url "leichen.png"
  ]
  node [
    id 8
    label "10455:CSE:cding"
    name "DING, Cunsheng"
    area "TH"
    url "cding.png"
  ]
  node [
    id 9
    label "10740:CSE:cktang"
    name "TANG, Chi-Keung"
    area "VG"
    url "cktang.png"
  ]
  node [
    id 10
    label "13781:CSE:yike"
    name "YI, Ke"
    area "TH"
	area2 "DB"
    url "yike"
  ]
  node [
    id 11
    label "10133:CSE:arya"
    name "ARYA, Sunil"
    area "TH"
    url "arya.png"
  ]
  node [
    id 12
    label "10029:CSE:achung"
    name "CHUNG, Albert Chi-Shing"
    area "VG"
	area2 "AI"
	
    url "achung.png"
  ]
  node [
    id 13
    label "12445:CSE:lzhang"
    name "ZHANG, Nevin Lianwen"
    area "AI"
    url "lzhang.png"
  ]
  node [
    id 14
    label "12356:CSE:lingu"
    name "GU, Lin"
    area "NE"
    url "lingu.png"
  ]
  node [
    id 15
    label "13095:CSE:qyang"
    name "YANG, Qiang"
    area "AI"
    url "qyang.png"
  ]
  node [
    id 16
    label "13086:CSE:quan"
    name "QUAN, Long"
    area "VG"
    url "quan.png"
  ]
  node [
    id 17
    label "12175:CSE:kwtleung"
    name "LEUNG, Wai Ting"
    area "DB"
    url "kwtleung.png"
  ]
  node [
    id 18
    label "10250:CSE:bli"
    name "LI, Bo"
    area "NE"
    url "bli.png"
  ]
  node [
    id 19
    label "13608:CSE:wng"
    name "wng"
    area ""
    url ""
  ]
  node [
    id 20
    label "10252:CSE:bmak"
    name "MAK, Brian Kan-Wing"
    area "AI"
    url "bmak.png"
  ]
  node [
    id 21
    label "12431:CSE:luo"
    name "LUO, Qiong"
    area "DB"
    url "luo.png"
  ]
  node [
    id 22
    label "13103:CSE:raywong"
    name "WONG, Raymond Chi-Wing"
    area "DB"
    url "raywong.png"
  ]
  node [
    id 23
    label "11634:CSE:horner"
    name "HORNER, Andrew B."
    area "ST"
    url "horner.png"
  ]
  node [
    id 24
    label "10882:CSE:dimitris"
    name "PAPADIAS, Dimitris"
    area "DB"
    url "dimitris.png"
  ]
  node [
    id 25
    label "13419:CSE:taicl"
    name "TAI, Chiew-Lan"
    area "VG"
    url "taicl.png"
  ]
  node [
    id 26
    label "11680:CSE:hunkim"
    name "KIM, Sunghun"
    area "ST"
    url "hunkim.png"
  ]
  node [
    id 27
    label "14055:CSE:panhui"
    name "HUI, Pan"
    area "NE"
    url "panhui.png"
  ]
  node [
    id 28
    label "13226:CSE:sccheung"
    name "CHEUNG, Shing-Chi"
    area "ST"
    url "sccheung.png"
  ]
  node [
    id 29
    label "11452:CSE:gchan"
    name "CHAN, Shueng-Han Gary"
    area "NE"
    url "gchan.png"
  ]
  node [
    id 30
    label "12828:CSE:ni"
    name "NI, Lionel M."
    area "NE"
    url "ni.png"
  ]
  node [
    id 31
    label "11465:CSE:gibson"
    name "LAM, Gibson"
    area "ST"
    url "gibson.png"
  ]
  node [
    id 32
    label "13228:CSE:scheng"
    name "CHENG, Siu-Wing"
    area "TH"
    url "scheng.png"
  ]
  node [
    id 33
    label "11856:CSE:jinzh"
    name "jinzh"
    area ""
    url "cse.png"
  ]
  node [
    id 34
    label "13142:CSE:rossiter"
    name "ROSSITER, David Paul"
    area "ST"
    url "rossiter.png"
  ]
  node [
    id 35
    label "11510:CSE:hamdi"
    name "HAMDI, Mounir"
    area "NE"
    url "hamdi.png"
  ]
  node [
    id 36
    label "13041:CSE:psander"
    name "SANDER, Pedro"
    area "VG"
    url "psander.png"
  ]
  node [
    id 37
    label "11795:CSE:jamesk"
    name "KWOK, James Tin-Yau"
    area "AI"
    url "jamesk.png"
  ]
  node [
    id 38
    label "11663:CSE:huamin"
    name "QU, Huamin"
    area "VG"
    url "huamin.png"

  ]
  node [
    id 39
    label "12390:CSE:lixin"
    name "LI, Xin"
    area "NE"
    url "lixin.png"
  ]
  node [
    id 40
    label "13379:CSE:stavrosp"
    name "stavrosp"
    area ""
    url "cse.png"
  ]
  node [
    id 41
    label "13070:CSE:qianzh"
    name "ZHANG, Qian"
    area "NE"
    url "qianzh.png"
  ]
  node [
      id 42
      label "6349:CSE:tfchan"
      name "CHAN, Tony F."
      area "dean"
      url "tfchan.png"
  ]
  node [
        id 43
        label "3511:CSE:flin"
        name "LIN, Fangzhen"
        area "AI"
        url "flin.png"
   ]
   node [
           id 44
           label "3544:CSE:tcpong"
           name "PONG, Ting-Chuen"
           area "VG"
           url "tcpong.png"
   ]
   node [
          id 45
          label "3539:CSE:dekai"
          name "WU, Dekai"
          area "AI"
          url "dekai.png"
   ]
   node [
          id 46
          label "3537:CSE:csbb"
          name "BENSAOU, Brahim"
          area "NE"
          url "csbb.png"
   ]
   node [
       id 47
       label "3505:CSE:wilfred"
       name "NG, Wilfred Siu-Hung"
       area "DB"
       url "wilfred.png"
     ]
   node [
       id 48
       label "3505:CSE:charlesz"
       name "ZHANG, Charles"
       area "ST"
       url "charlesz.png"
     ]
  node [
       id 49
       label "3546:CSE:kaichen"
       name "CHEN, Kai"
       area "ST"
       url "kaichen.png"
     ]
  edge [
    source 0
    target 13
    weight "3"
  ]
  edge [
    source 0
    target 1
    weight "7"
  ]
  edge [
    source 0
    target 15
    weight "2"
  ]
  edge [
    source 0
    target 16
    weight "3"
  ]
  edge [
    source 0
    target 19
    weight "2"
  ]
  edge [
    source 0
    target 37
    weight "18"
  ]
  edge [
    source 0
    target 12
    weight "1"
  ]
  edge [
    source 1
    target 15
    weight "1"
  ]
  edge [
    source 1
    target 2
    weight "5"
  ]
  edge [
    source 2
    target 13
    weight "1"
  ]
  edge [
    source 2
    target 17
    weight "12"
  ]
  edge [
    source 2
    target 18
    weight "4"
  ]
  edge [
    source 2
    target 19
    weight "9"
  ]
  edge [
    source 2
    target 21
    weight "4"
  ]
  edge [
    source 2
    target 24
    weight "2"
  ]
  edge [
    source 3
    target 24
    weight "3"
  ]
  edge [
    source 3
    target 14
    weight "5"
  ]
  edge [
    source 3
    target 41
    weight "3"
  ]
  edge [
    source 3
    target 7
    weight "16"
  ]
  edge [
    source 3
    target 15
    weight "1"
  ]
  edge [
    source 3
    target 28
    weight "1"
  ]
  edge [
    source 3
    target 30
    weight "57"
  ]
  edge [
    source 3
    target 21
    weight "4"
  ]
  edge [
    source 3
    target 10
    weight "2"
  ]
  edge [
    source 4
    target 9
    weight "1"
  ]
  edge [
    source 4
    target 16
    weight "2"
  ]
  edge [
    source 4
    target 37
    weight "1"
  ]
  edge [
    source 4
    target 12
    weight "5"
  ]
  edge [
    source 5
    target 8
    weight "1"
  ]
  edge [
    source 5
    target 18
    weight "4"
  ]
  edge [
    source 5
    target 11
    weight "1"
  ]
  edge [
    source 5
    target 32
    weight "4"
  ]
  edge [
    source 6
    target 31
    weight "1"
  ]
  edge [
    source 6
    target 13
    weight "3"
  ]
  edge [
    source 6
    target 34
    weight "1"
  ]
  edge [
    source 6
    target 28
    weight "1"
  ]
  edge [
    source 6
    target 35
    weight "16"
  ]
  edge [
    source 7
    target 41
    weight "2"
  ]
  edge [
    source 7
    target 27
    weight "2"
  ]
  edge [
    source 7
    target 30
    weight "8"
  ]
  edge [
    source 7
    target 21
    weight "1"
  ]
  edge [
    source 7
    target 22
    weight "1"
  ]
  edge [
    source 7
    target 10
    weight "4"
  ]
  edge [
    source 9
    target 16
    weight "1"
  ]
  edge [
    source 9
    target 12
    weight "1"
  ]
  edge [
    source 9
    target 32
    weight "1"
  ]
  edge [
    source 10
    target 21
    weight "3"
  ]
  edge [
    source 10
    target 32
    weight "2"
  ]
  edge [
    source 11
    target 32
    weight "3"
  ]
  edge [
    source 12
    target 38
    weight "2"
  ]
  edge [
    source 13
    target 15
    weight "1"
  ]
  edge [
    source 14
    target 30
    weight "2"
  ]
  edge [
    source 14
    target 28
    weight "2"
  ]
  edge [
    source 15
    target 28
    weight "1"
  ]
  edge [
    source 15
    target 18
    weight "1"
  ]
  edge [
    source 15
    target 30
    weight "4"
  ]
  edge [
    source 15
    target 37
    weight "7"
  ]
  edge [
    source 15
    target 21
    weight "1"
  ]
  edge [
    source 15
    target 23
    weight "1"
  ]
  edge [
    source 16
    target 37
    weight "1"
  ]
  edge [
    source 16
    target 25
    weight "3"
  ]
  edge [
    source 17
    target 19
    weight "6"
  ]
  edge [
    source 18
    target 33
    weight "5"
  ]
  edge [
    source 18
    target 35
    weight "2"
  ]
  edge [
    source 18
    target 30
    weight "1"
  ]
  edge [
    source 18
    target 41
    weight "30"
  ]
  edge [
    source 19
    target 22
    weight "1"
  ]
  edge [
    source 20
    target 31
    weight "1"
  ]
  edge [
    source 20
    target 34
    weight "1"
  ]
  edge [
    source 20
    target 37
    weight "8"
  ]
  edge [
    source 21
    target 28
    weight "2"
  ]
  edge [
    source 21
    target 36
    weight "6"
  ]
  edge [
    source 21
    target 30
    weight "11"
  ]
  edge [
    source 21
    target 38
    weight "2"
  ]
  edge [
    source 23
    target 34
    weight "1"
  ]
  edge [
    source 24
    target 40
    weight "15"
  ]
  edge [
    source 24
    target 35
    weight "1"
  ]
  edge [
    source 26
    target 28
    weight "3"
  ]
  edge [
    source 28
    target 30
    weight "5"
  ]
  edge [
    source 28
    target 38
    weight "1"
  ]
  edge [
    source 29
    target 37
    weight "1"
  ]
  edge [
    source 29
    target 41
    weight "11"
  ]
  edge [
    source 30
    target 33
    weight "1"
  ]
  edge [
    source 30
    target 35
    weight "3"
  ]
  edge [
    source 30
    target 38
    weight "6"
  ]
  edge [
    source 30
    target 41
    weight "16"
  ]
  edge [
    source 31
    target 34
    weight "13"
  ]
  edge [
    source 33
    target 41
    weight "21"
  ]
  edge [
    source 33
    target 35
    weight "2"
  ]
  edge [
    source 35
    target 39
    weight "9"
  ]
  edge [
    source 35
    target 41
    weight "10"
  ]
]
